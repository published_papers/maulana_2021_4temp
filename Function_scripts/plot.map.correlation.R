###plots a correlation matrix of a genetic map for visual inspection

###input
#correlation_matrix (any correlation or identity matrix)
#strain.marker (of the invesigated strains: markers in rows, with columns name, chromosome and position)
#chromosomes (character vector with chromosome names; standard is C. elegans)
#chromosome_size (numeric vector with chromosome sizes; standard is C. elegans)
#relative (whether plots should be sized relative to chromosome size)
#plotted_value (y-axis description title)


###output
#plot of genetic correlation between markers per chromosome


###Description


plot.map.correlation <- function(correlation_matrix,strain.marker,chromosomes,chromosome_size,relative=TRUE,plotted_value){
                                  if(missing(correlation_matrix)){                      stop("Give a correlation matrix as input")}
                                  if(missing(strain.marker)){                           stop("Give the marker info file (name, chr, bp)")}
                                  if(missing(chromosomes)){                             chromosomes <- c("I","II","III","IV","V","X")}
                                  if(missing(chromosome_size)){                         chromosome_size <- c(15072434,15279421,13783801,17493829,20924180,17718942)}
                                  if(length(chromosomes) != length(chromosome_size)){   stop("chromosome names and sizes should have the same length")}
                                  if(missing(plotted_value)){                           plotted_value <- "Correlation"}

                                  ###input chromosomes  
                                  chr.info <- data.frame(cbind(chromosome=chromosomes,
                                                               size=chromosome_size,
                                                               cumulative=c(0,cumsum(chromosome_size[-length(chromosome_size)]))),
                                                         stringsAsFactors=FALSE)
                                  chr.info[,2] <- as.numeric(as.character(unlist(chr.info[,2]))) 
                                  chr.info[,3] <- as.numeric(as.character(unlist(chr.info[,3])))
                                  
                                  ###The arabidopsis chromosomes
                                  #chr.info <- data.frame(cbind(chromosome=1:5,
                                  #                             size=c(30427671,19698289,23459830,18585056,26975502),
                                  #                             cumulative=c(0,30427671,50125960,73585790,92170846)),
                                  #                       stringsAsFactors=FALSE)
                                  #chr.info[,2] <- as.numeric(as.character(unlist(chr.info[,2]))) 
                                  #chr.info[,3] <- as.numeric(as.character(unlist(chr.info[,3])))
                                  
                                  ###manipulate input to right type
                                  strain.marker[,1] <- as.character(unlist(strain.marker[,1]))
                                  strain.marker[,2] <- as.character(unlist(strain.marker[,2]))
                                  strain.marker[,3] <- as.numeric(as.character(unlist(strain.marker[,3])))

                                  ###color scale generator
                                    color.scale <- function(colors.use,y.val){
                                                            if(class(y.val) == "matrix"){
                                                                scale.use <-  c(as.numeric(rownames(y.val))[nrow(y.val)],as.numeric(rownames(y.val))[1],as.numeric(rownames(y.val))[1]-as.numeric(rownames(y.val))[2])
                                                            }
                                                            if(class(y.val) == "numeric"){
                                                                scale.use <- c(min(y.val,na.rm=T),max(y.val,na.rm=T),diff(c(min(y.val,na.rm=T),max(y.val,na.rm=T)))/100)
                                                            }
                                                            seq1 <- seq(scale.use[1],scale.use[2],scale.use[3])    ###adjust for optimal effect
                                                            ramp <- colorRamp(colors.use, bias=1)
                                                            colors.out <- rgb(ramp(seq(0, 1, length = length(seq1))), max = 255)
                                                            return(colors.out)
                                                           }

                                  correlation_matrix[upper.tri(correlation_matrix)] <- NA
                                  y.val <- c(-1,1)
                                  seq1 <- seq(-1,1,0.05)    ###adjust for optimal effect
                                  colors.use <- c("#8E0152","#C51B7D","#DE77AE","#F1B6DA","#FDE0EF","#F7F7F7","#E6F5D0","#B8E186","#7FBC41","#4D9221","#276419")
                                  colors.use <- color.scale(colors.use,y.val)

                                  rel.size <- chr.info[,2]/(chr.info[nrow(chr.info),3]+chr.info[nrow(chr.info),2])
                                  rel.size <- c(0,cumsum(rel.size))

                                  if(relative){
                                      for(i in 1:nrow(chr.info)){
                                          for(j in 1:nrow(chr.info)){
                                              chr.i <- chr.info[i,1]
                                              chr.j <- chr.info[j,1]
                                              current.data <- as.matrix(correlation_matrix[strain.marker[,2]==chr.i,strain.marker[,2]==chr.j])
                                              if(i == 1 & j==1){par(fig=c(rel.size[i],rel.size[i+1],rel.size[j],rel.size[j+1]), omi=c(bottom=2,left=2,top=2,right=2),mar=c(bottom=0,left=0,top=0,right=0))}
                                              if(i != 1 & i>=j){par(fig=c(rel.size[i],rel.size[i+1],rel.size[j],rel.size[j+1]),new=T)}
                                              if(i >=j){
                                                  image(x=strain.marker[strain.marker[,2]==chr.i,3]/1000000, y=strain.marker[strain.marker[,2]==chr.j,3]/1000000,z=current.data,axes=F,xlab="",ylab="",zlim=y.val,col=colors.use)
                                                  box()
                                              }
                                              if(j ==1){
                                                  axis(1,seq(0,20,by=2.5),labels=F,tcl=-0.2)
                                                  axis(1,seq(10,20,by=10),labels=seq(10,20,by=10),mgp=c(3,0.7,0),cex.axis=1.5,tcl=-0.5,las=1)
                                              }
                                              if(i == nrow(chr.info)){
                                                  axis(4,seq(0,20,by=2.5),labels=F,tcl=-0.2)
                                                  axis(4,seq(10,20,by=10),labels=seq(10,20,by=10),mgp=c(3,0.7,0),cex.axis=1.5,tcl=-0.5,las=2)
                                              }
                                              if(j ==i){
                                                  mtext(side=3,text=chr.i,cex=1.5,line=-1.5)
                                              }
                                          }
                                      }

                                      par(fig=c(0,1,0,1),new=T)
                                          plot(0,0,type="n",xlab="",ylab="",axes=F)
                                          mtext(side=1,text="Location marker 1 (Mb)",cex=1.5,line=2.5)
                                          mtext(side=4,text="Location marker 2 (Mb)",cex=1.5,line=2.5)

                                  } else{
                                      for(i in 1:nrow(chr.info)){
                                          for(j in 1:nrow(chr.info)){
                                              chr.i <- chr.info[i,1]
                                              chr.j <- chr.info[j,1]
                                              current.data <- as.matrix(correlation_matrix[strain.marker[,2]==chr.i,strain.marker[,2]==chr.j])
                                              if(i == 1 & j==1){par(fig=c(rel.size[i],rel.size[i+1],rel.size[j],rel.size[j+1]), omi=c(bottom=2,left=2,top=2,right=2),mar=c(bottom=0,left=0,top=0,right=0))}
                                              if(i != 1 & i>=j){par(fig=c(rel.size[i],rel.size[i+1],rel.size[j],rel.size[j+1]),new=T)}
                                              if(i >=j){
                                                  image(x=1:length(strain.marker[strain.marker[,2]==chr.i,3]), y=1:length(strain.marker[strain.marker[,2]==chr.j,3]),z=current.data,axes=F,xlab="",ylab="",zlim=y.val,col=colors.use)
                                                  box()
                                              }
                                              if(j ==i){
                                                  mtext(side=3,text=chr.i,cex=1.5,line=-1.5)
                                              }
                                          }
                                      }

                                      par(fig=c(0,1,0,1),new=T)
                                          plot(0,0,type="n",xlab="",ylab="",axes=F)
                                          mtext(side=1,text="marker 1",cex=1.5,line=2.5)
                                          mtext(side=4,text="marker 2",cex=1.5,line=2.5)

                                  }



                                  par(fig=c(0,0.5,0.9,1),new=T)
                                      color.matrix <- as.matrix(seq1)
                                      color.matrix <- cbind(color.matrix,NA,NA)
                                      image(y=1:ncol(color.matrix),x=seq1,z=color.matrix,col=colors.use,zlim=y.val,xlab="",ylab="",axes=F)
                                      rect(xleft=min(seq1)-0.5, ybottom=0.5, xright=max(seq1)+0.5, ytop=1.5)

                                      axis(1,quantile(ceiling(seq1)),labels=quantile(ceiling(seq1)),mgp=c(3,0.7,0),cex.axis=1.5,tcl=-0.2,las=1)
                                      mtext(text=plotted_value, side=1, cex=1.5,line=2)
                                }

