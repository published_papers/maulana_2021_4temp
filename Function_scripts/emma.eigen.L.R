###emma package
#Kang, Hyun Min, et al. "Efficient control of population structure in model organism association mapping." Genetics 178.3 (2008): 1709-1723.
#emma 1.12


emma.eigen.L <- function (Z, K, complete = TRUE)
{
    if (is.null(Z)) {
        return(emma.eigen.L.wo.Z(K))
    }
    else {
        return(emma.eigen.L.w.Z(Z, K, complete))
    }
}
